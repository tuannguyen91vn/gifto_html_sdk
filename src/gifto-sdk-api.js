/*1508540788,,JIT Construction: v3389191,en_US*/

/**
 * Copyright (c) 2017-present, Gifto Wallet, Inc. All rights reserved.

 *	Features
 	-	GUI Home
 	-	Create Wallet
 	-	View Wallet address
 	-	Transfer Gifto
 	-	View transactin History

 *	APIs List:
 	-	Create Gifto Wallet using identity data and password
	-	Get Gifto Wallet Detail of an account using identity data
	-	Transfer Gifto from an account to another account using wallet address
	-	Transfer Gifto from an account to another account using identity data (Tipping)
	-	Get History of Transferring Gifto (Sent and Received, include Transfer and Tipping)
 */
(function(window){
	function GiftoSDKApi () {
		var _myGiftoSdk = {};

    	_myGiftoSdk.apiHost = 'https://wallet.gifto.io/apiv2/v2/';

		_myGiftoSdk.wallet = {
			identityData: null,
			passphrase: null,
			address: null,
			apikey: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3N1ZXIiOiJyb3NlY29pbmFkbWluIiwic3ViIjoicGFydG5lcl9hcGlfY2FsbCJ9.yy7vxDM4204gKv319RWC6fVlP2ZEikCHCRqkhSeVFuI='
		};

		_myGiftoSdk.currencyCode = 'RSC';
		_myGiftoSdk.currencyName = 'Gifto';
		/**
		*	SDK Excute API
		*/
		_myGiftoSdk.executeApi = function (api_method, api_url, api_request, callback_function) {

			var xhttp = new XMLHttpRequest();
		    
		    xhttp.open(api_method, api_url, true);
		    
		    xhttp.setRequestHeader("Content-type", "application/json");

		    xhttp.setRequestHeader("Authorization", _myGiftoSdk.wallet.apikey);	

		    var response = {};

		  	//== Call API finish
		    xhttp.onreadystatechange = function() {
			    if(xhttp.readyState == XMLHttpRequest.DONE ) {
			    	if (xhttp.status == 200) {
			    		response = JSON.parse(xhttp.responseText);

			    	} else if ( xhttp.status == 201 ) {
			    		
			    		response = {'message' : 'Successfully with No data response'};

			    	} else if ( xhttp.status == 404 ) {
						
						response = {'message' : 'Error: Wallet indentity not found'};

			    	} else if ( xhttp.status == 202 ) {
			    		response = {'message': 'Transfer Accepted'}
			    	}

			    	if (typeof callback_function == 'function') { 
					  	callback_function(response); 
					}
			    }
			}

		    xhttp.send(JSON.stringify(api_request));
		   	
		}

		/**
		*	SDK Verify response
		*/
		_myGiftoSdk.varifyResponse = function (apiResponse) {

			return apiResponse;
		}

		/**
		*	SDK Init
		*/
		_myGiftoSdk.init = function (options) {
			_myGiftoSdk.wallet.identityData = options.email;
			_myGiftoSdk.wallet.passphrase = options.password;
			_myGiftoSdk.wallet.apikey = options.apikey;
		}

		/**
		*	SDK Create Wallet
		*/
		 _myGiftoSdk.createWallet = function (data, callback) {
		 	var apiMethod = 'POST';

		 	var apiURL = this.apiHost + 'wallets/create';
		 	
		 	var apiRequest = {
		 		"identityData": data.identityData,
				"firstname": data.firstname, 
				"lastname": data.lastname,
				"passphrase": data.passphrase,
				"currencyCode": _myGiftoSdk.currencyCode 
		 	};

		 	var apiResult = this.executeApi(apiMethod, apiURL, apiRequest, callback);

		 	return apiResult;

		}

		/**
		*	SDK Wallet Detail
		*/
		_myGiftoSdk.detailWallet = function (data, callback) {

		 	var apiMethod = 'POST';

		 	var apiURL = _myGiftoSdk.apiHost + 'wallets/detail';
		 	
		 	var apiRequest = {
		 		"identityData": data.identityData
		 	};

		 	var apiResult = _myGiftoSdk.executeApi(apiMethod, apiURL, apiRequest, callback);

		 	return apiResult;

		}

		/**
		*	SDK Coin Transfer
		*/
		_myGiftoSdk.transferCoin = function (data, callback) {
		 	
		 	var apiMethod = 'POST';

		 	var apiURL = this.apiHost + 'payments/transfer';
		 	
		 	var apiRequest = {
		 		"identityData": data.identityData,
				"currencyCode": data.currencyCode,
				"passphrase": data.passphrase,

				"toWalletAddress": data.toWalletAddress,
				"amount": data.amount,
				"referenceId": data.referenceId,
				"note": data.note,
				"transferFeeType": data.transferFeeType,
				"type": data.type
		 	};

		 	var apiResult = this.executeApi(apiMethod, apiURL, apiRequest, callback);

		 	return apiResult;

		}

		/**
		*	SDK Transactions History
		*/
		_myGiftoSdk.listTransactions = function (data, callback) {
		 	var offset = data.offset != null ? data.offset : 0;
		 	var limit = data.limit != null ? data.limit : 50;

		 	var apiMethod = 'POST';

		 	var apiURL = this.apiHost + 'payments/transactions/transfer/list';
		 	
		 	var apiRequest = {
		 		"identityData": data.identityData,
		 		"offset": offset,
    			"limit": limit,
    			"transactionType": data.transactionType, //Supported type : all/tranfer/tip/withdraw
    			"transactionMode": data.transactionsMode // Supported mode " send/receive
		 	};

		 	var apiResult = this.executeApi(apiMethod, apiURL, apiRequest, callback);

		 	return apiResult;

		}

    	return _myGiftoSdk;
	}

	// Create globally accesible in the window
	if(typeof(window.GiftoSDKApi) === 'undefined'){
		window.GiftoSDKApi = GiftoSDKApi();
	}

})(window);